package com.aerialdevelopment.android.tabata;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.PatternSyntaxException;

import android.os.Environment;

public class ImportExportUtility {

	private static ImportExportUtility instance = null;
	
	public static final String DIR_NAME = "ExtendedTabata";
	
	private String cycleString = null;
	
	
	public ImportExportUtility(){
		super();
	}
	
	//Singleton
	public static ImportExportUtility getInstance(){
		if(instance==null)
			instance = new ImportExportUtility();
		return instance;
	}
	
	//=====================================================
	
	//RETURNS:
	//  -1 - bad regexp
	//  0 - exists
	//  1 - free
	public static int isNameAailable(String name){
		String regexp="[a-zA-Z0-9 /-_!]{1,20}";
		if(!name.matches(regexp))
			return -1;
		
		//najpierw sprawdzamy,czy jest directory
		File directory = new File(Environment.getExternalStorageDirectory()+"/"+DIR_NAME);
		if(!directory.exists() || !directory.isDirectory()) // nie ma, wiec mozna tworzyc co sie chce
			return 1;
		else{
			File file = new File(Environment.getExternalStorageDirectory()+"/"+DIR_NAME, name);
			if(file.exists())
				return 0;
			else 
				return 1;
		}
	}
	
	//RETURNS:
	// 1 - if is mounted and ready to write
	// 0 - read only
	// -1 - not mounted
	public static int checkAvailability(){
		boolean mExternalStorageAvailable = false;
		boolean mExternalStorageWriteable = false;
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
		    // We can read and write the media
		    mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
		    // We can only read the media
		    mExternalStorageAvailable = true;
		    mExternalStorageWriteable = false;
		} else {
		    // Something else is wrong. It may be one of many other states, but all we need
		    //  to know is we can neither read nor write
		    mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
		
		if(mExternalStorageAvailable && mExternalStorageWriteable)
			return 1;
		else if(mExternalStorageAvailable && !mExternalStorageWriteable)
			return 0;
		else 
			return -1;
	}
	
	
	//RETURN:
	// 1 - can write and read
	// 0 - can read only
	// -1 - can't do anything
	public static int checkWritingPrivilages(){
		int ret = -1;
		File routesRoot = Environment.getExternalStorageDirectory();
		if (routesRoot.canRead()){
			ret = 0;
			if (routesRoot.canWrite())
				ret = 1;
		}
		return ret;
	}
	
	public boolean saveCycle(String content, String fileName){
		File directory = new File(Environment.getExternalStorageDirectory()+"/"+DIR_NAME);
		if(!directory.exists() || !directory.isDirectory())
			directory.mkdirs();
		
		File file = new File(Environment.getExternalStorageDirectory()+"/"+DIR_NAME, fileName+".txt");
		try {
			FileOutputStream fos = new FileOutputStream(file);
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fos));
			writer.write(content);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public ArrayList<String> listAvailableFiles(){
		String[] list = null;
		File directory = new File(Environment.getExternalStorageDirectory()+"/"+DIR_NAME);
		if(directory!=null && directory.exists() && directory.isDirectory())
			list = directory.list();
		if(list!=null)
			return new ArrayList<String>(Arrays.asList(list));
		else
			return new ArrayList<String>();
	}
	
	public boolean deleteFile(String fileName){
		File file = new File(Environment.getExternalStorageDirectory()+"/"+DIR_NAME, fileName);
		return file.delete();
	}
	
	public ArrayList<String> openfile(String fileName){
		ArrayList<String> result = new ArrayList<String>();
		try{
			File file = new File(Environment.getExternalStorageDirectory()+"/"+DIR_NAME, fileName);
			BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
			String line = "";
			while ((line = input.readLine()) != null) {
				result.add(line);
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (PatternSyntaxException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
}
