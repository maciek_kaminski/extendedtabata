package com.aerialdevelopment.android.tabata;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class OpenListActivity extends ListActivity {
	
	OpenFileArrayAdapter adapter;
	ArrayList<String> names;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.opencyclelayout);
		
		names = ImportExportUtility.getInstance().listAvailableFiles();

		adapter = new OpenFileArrayAdapter(this, names);
		setListAdapter(adapter);
		
		Button back = (Button) this.findViewById(R.id.backButton);
		back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(RESULT_CANCELED);
		    	finish();
			}
		});
	}


	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		// Get the item that was clicked
//		Object o = this.getListAdapter().getItem(position);
//		String keyword = o.toString();
//		Toast.makeText(this, "You selected: " + keyword, Toast.LENGTH_LONG)
//				.show();
		Button open = (Button) v.findViewById(R.id.openCycleFile);
		open.setVisibility(View.VISIBLE);
		adapter.setSelectedPosition(position);
		adapter.notifyDataSetChanged();
	}
	
	public void remveFileFromList(String object){
		adapter.remove(object);
		adapter.notifyDataSetChanged();
	}
	
	
	
	//=====================================================================================
	public class OpenFileArrayAdapter extends ArrayAdapter<String> {

		private final Activity context;
		private int selectedPosition=-1;
		
		public OpenFileArrayAdapter(Activity context, ArrayList<String> list) {
			super(context, R.layout.cyclefilelayout, R.id.label, list);
			this.context = context;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (view == null) {
				LayoutInflater inflator = context.getLayoutInflater();
				view = inflator.inflate(R.layout.cyclefilelayout, null);
			} 
			
			Button open = (Button) view.findViewById(R.id.openCycleFile);
			Button delete = (Button) view.findViewById(R.id.deleteCycleFile);
			
			final String fileName = this.getItem(position);
			if(fileName!=null && !fileName.equals("")){
				TextView text = (TextView) view.findViewById(R.id.label);
				
				text.setText(fileName);
				
				if(position==selectedPosition){
					open.setVisibility(View.VISIBLE);
					delete.setVisibility(View.VISIBLE);
				}else{
					open.setVisibility(View.INVISIBLE);
					delete.setVisibility(View.INVISIBLE);
				}
			}
			
			//OPEN BUTTON
			open.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					ArrayList<String> tabatas = ImportExportUtility.getInstance().openfile(fileName);
					
					Bundle bundle = new Bundle();
		    		bundle.putStringArrayList(MainTabata.BUNDLE_OPEN_TABATA_LIST, tabatas);
			    	Intent mIntent = new Intent();
			    	mIntent.putExtras(bundle);
			    	setResult(RESULT_OK, mIntent);
			    	finish();
				}
			});
			
			//DELETE BUTTON
			delete.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					AlertDialog.Builder builder = new AlertDialog.Builder(OpenListActivity.this);
					builder.setMessage(R.string.deleteFileConfirm)
					       .setCancelable(false)
					       .setPositiveButton(R.string.dialogClearYes, new DialogInterface.OnClickListener() {
					           public void onClick(DialogInterface dialog, int id) {
					        	   if(ImportExportUtility.getInstance().deleteFile(fileName)){
					        		   Toast.makeText(context, R.string.fileDeleted, Toast.LENGTH_LONG).show();
					        		   remveFileFromList(fileName);
					        	   }else
					        		   Toast.makeText(context, R.string.fileNotDeleted, Toast.LENGTH_LONG).show();
					           }
					       })
					       .setNegativeButton(R.string.dialogClearNo, new DialogInterface.OnClickListener() {
					           public void onClick(DialogInterface dialog, int id) {
					                dialog.cancel();
					           }
					       });
					AlertDialog alert = builder.create();
					alert.show();
				}
			});
			
			return view;
		}
		
		
		public void setSelectedPosition(int position){
			this.selectedPosition = position;
		}
		
	}
}
