package com.aerialdevelopment.android.tabata;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class TabataAdapter extends ArrayAdapter<Tabata> {

    private ArrayList<Tabata> items;
    private Context context;
    private int nrToColor=0;

    public TabataAdapter(Context context, int textViewResourceId, ArrayList<Tabata> items) {
            super(context, textViewResourceId, items);
            this.context = context;
            this.items = items;
    }
    
    

//    @Override
//	public int getCount() {
//		//return super.getCount();
//    	return items.size();
//	}

    public void setNrToColor(int nr){
    	this.nrToColor = nr;
    }
    

	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.onetabataview, null);
            }
            Tabata o = items.get(position);
            if (o != null) {
                    TextView title = (TextView) v.findViewById(R.id.oneTabataTitle);
                    TextView time = (TextView) v.findViewById(R.id.oneTabataTime);
                    ImageView arrow = (ImageView) v.findViewById(R.id.oneTabataArrow);
                    if (title != null)
                         title.setText(o.getTitle());
                    if(time != null) {
                        //time.setText(Long.valueOf(o.getTime()).toString());
                        time.setText(AppUtils.generateTimeFromLong(o.getTime()));
                    }
                    if (arrow != null && position>0)
                    	arrow.setVisibility(View.VISIBLE);
                    else
                    	arrow.setVisibility(View.GONE);
                    //color
                    setTabataBoarder((LinearLayout)v.findViewById(R.id.oneTabataViewBoarder), (position==this.nrToColor)?true:false);
            }
            return v;
    }
	
	
	public void setTabataBoarder(LinearLayout layout, boolean selected){
		if(selected)
			layout.setBackgroundColor(Color.parseColor("#FF0000"));
		else
			layout.setBackgroundColor(Color.parseColor("#00aa00"));
	}
}
