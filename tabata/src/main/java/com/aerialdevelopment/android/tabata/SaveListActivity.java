package com.aerialdevelopment.android.tabata;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SaveListActivity extends Activity {
	
	private EditText title;
	private String cycleString;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.savetabata);
		
		title = (EditText)findViewById(R.id.cycleTitle);
		Button cancel = (Button)findViewById(R.id.saveCancel);
		Button save = (Button)findViewById(R.id.saveOk);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			cycleString = extras.getString(MainTabata.BUNDLE_SAVE_TITLE);
		}
		
		//SAVE
		save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(cycleString==null ||  cycleString.equals("")){
					Toast.makeText(SaveListActivity.this, R.string.emptyStringError, Toast.LENGTH_SHORT).show();
					return;
				}
				
				if(title.getText()==null ||  title.getText().toString().equals("")){
					Toast.makeText(SaveListActivity.this, R.string.lackOfCycleTitle, Toast.LENGTH_SHORT).show();
					return;
				}
				
				String titleString = title.getText().toString();
				int availability = ImportExportUtility.isNameAailable(titleString);
				if(availability==-1){
					Toast.makeText(SaveListActivity.this, R.string.regexpCycleName, Toast.LENGTH_SHORT).show();
					return;
				} else if(availability==0){
					Toast.makeText(SaveListActivity.this, R.string.takenCycleName, Toast.LENGTH_SHORT).show();
					return;
				}
				
				//mozemy zapisywac
				boolean result = saveCycle(cycleString, titleString);
				
				Bundle bundle = new Bundle();
		    		bundle.putBoolean(MainTabata.BUNDLE_SAVE_TITLE, result);
		    	Intent mIntent = new Intent();
		    	mIntent.putExtras(bundle);
		    	setResult(RESULT_OK, mIntent);
		    	finish();
			}
		});
		
		//CANCEL
		cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(RESULT_CANCELED, null);
    	        finish();
			}
		});
	}
	
	private boolean saveCycle(String cycleString, String titleString) {
		return ImportExportUtility.getInstance().saveCycle(cycleString, titleString);
	}
}
