package com.aerialdevelopment.android.tabata;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class AddEditTabata extends Activity {

    private EditText titleEdit;
    private int editedId = -1;
    private NumberPicker timeSecondsNumberPicker;
    private NumberPicker timeMinutesNumberPicker;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addedittabata);
		
		titleEdit = (EditText)findViewById(R.id.tabataTitle);

        timeSecondsNumberPicker = (NumberPicker)findViewById(R.id.tabataSecondsTimeNumberPicker);
            timeSecondsNumberPicker.setMinValue(0);
            timeSecondsNumberPicker.setMaxValue(AppUtils.SECONDS_IN_MINUTE-1);
            timeSecondsNumberPicker.setValue(0);
        timeMinutesNumberPicker = (NumberPicker)findViewById(R.id.tabataMinutesTimeNumberPicker);
            timeMinutesNumberPicker.setMinValue(0);
            timeMinutesNumberPicker.setMaxValue(AppUtils.SECONDS_IN_MINUTE-1);
            timeMinutesNumberPicker.setValue(0);

		Button cancel = (Button)findViewById(R.id.cancelTabata);
		Button ok = (Button)findViewById(R.id.okTabata);
		
		//Jesli to edit, a nie add
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			titleEdit.setText(extras.getString(MainTabata.BUNDLE_EDIT_TITLE));
			//timeEdit.setText(String.valueOf(extras.getLong(MainTabata.BUNDLE_EDIT_TIME)));
            setTimeOnView(extras.getLong(MainTabata.BUNDLE_EDIT_TIME));
			editedId = extras.getInt(MainTabata.BUNDLE_EDIT_ID);
		}
		
		//OK
		ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(getTimeFromView()==0l){
					Toast.makeText(AddEditTabata.this, R.string.lackOfTabataTimeToast, Toast.LENGTH_SHORT).show();
					return;
				}
				Bundle bundle = new Bundle();
					if(editedId>=0)
						bundle.putInt(MainTabata.BUNDLE_EDIT_ID, editedId);
		    		bundle.putString(MainTabata.BUNDLE_EDIT_TITLE, titleEdit.getText().toString().equals("") ? "tabata" : titleEdit.getText().toString());
		    		bundle.putLong(MainTabata.BUNDLE_EDIT_TIME, getTimeFromView());
		    	Intent mIntent = new Intent();
		    	mIntent.putExtras(bundle);
		    	setResult(RESULT_OK, mIntent);
		    	finish();
			}
		});
		
		//CANCEL
		cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(RESULT_CANCELED, null);
    	        finish();
			}
		});
	}

    private void setTimeOnView(long time) {
        if(timeMinutesNumberPicker==null || timeSecondsNumberPicker==null){
            return;
        }
        long minutes = time / AppUtils.SECONDS_IN_MINUTE;
        if(minutes>timeMinutesNumberPicker.getMaxValue()){
            timeMinutesNumberPicker.setValue(timeMinutesNumberPicker.getMaxValue());
        }else {
            timeMinutesNumberPicker.setValue((int)minutes);
        }

        long seconds = time % AppUtils.SECONDS_IN_MINUTE;
        if(seconds>timeSecondsNumberPicker.getMaxValue()){
            timeSecondsNumberPicker.setValue(timeSecondsNumberPicker.getMaxValue());
        }else {
            timeSecondsNumberPicker.setValue((int)seconds);
        }
    }

    private Long getTimeFromView(){
        if(timeMinutesNumberPicker==null || timeSecondsNumberPicker==null){
            return 0l;
        }
        Long value = (long) (timeMinutesNumberPicker.getValue() * AppUtils.SECONDS_IN_MINUTE + timeSecondsNumberPicker.getValue());

        return value;
    }

}
