package com.aerialdevelopment.android.tabata;

import android.widget.LinearLayout;

public class Tabata {

	private String title;
	private long time;
	
	public Tabata(String title, long time) {
		super();
		this.title = title;
		this.time = time;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
}
