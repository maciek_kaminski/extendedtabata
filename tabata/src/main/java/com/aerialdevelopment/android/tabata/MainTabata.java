package com.aerialdevelopment.android.tabata;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainTabata extends Activity {

	private static final int ACTIVITY_CREATE=0;
    private static final int ACTIVITY_EDIT= Menu.FIRST;
    private static final int ACTIVITY_DELETE = Menu.FIRST +1;
    private static final int ACTIVITY_SELECT = Menu.FIRST +2;
    private static final int ACTIVITY_OPEN = Menu.FIRST + 3;
    private static final int ACTIVITY_SAVE = Menu.FIRST + 4;
    
    
    public static final String BUNDLE_EDIT_ID = "bundle_edit_id";
    public static final String BUNDLE_EDIT_TITLE = "bundle_edit_title";
    public static final String BUNDLE_EDIT_TIME = "bundle_edit_time";
    
    public static final String BUNDLE_SAVE_TITLE = "bundle_save";
    public static final String BUNDLE_OPEN = "bundle_open";
    public static final String BUNDLE_OPEN_TABATA_LIST = "bundle_open_tabata_list";
    
    
	private Handler mHandler = new Handler();
	private CountTimer timer=null;
	private SoundManager soundManager;
	
	private TabataAdapter tabataListAdapter;
	private ArrayList<Tabata> tabataObjectList;
	
	private ImportExportUtility importUtility = ImportExportUtility.getInstance();
	
	private LayoutInflater inflater = null;
	private ListView listLayout;
	private TextView labelCount;
	private TextView labelTotalCount;
	private TextView labelTitle;
	
	public int actualManagedIndex = 0;
	private boolean running = false;

	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		setContentView(R.layout.main);
		
		labelCount = (TextView) findViewById(R.id.label_count);
		labelTotalCount = (TextView) findViewById(R.id.label_total);
		labelTitle = (TextView) findViewById(R.id.label_title);
		listLayout = (ListView) findViewById(R.id.listLayout);
		Button startButton = (Button) findViewById(R.id.sp);
		Button addButton = (Button) findViewById(R.id.add);
		
		tabataObjectList = new ArrayList<Tabata>();
		tabataListAdapter = new TabataAdapter(this, R.layout.onetabataview, tabataObjectList);
		listLayout.setAdapter(tabataListAdapter);
		registerForContextMenu(listLayout);
		
		//init variables and add sounds
		prepareSoundPlayers();
		
		AppUtils.getInstance(this).setKeepScreenOn(this, true);
		
/*		addNewTabata("Regular pushups",60);
		addNewTabata("Hip pushups",45);
		addNewTabata("Pause",45);
		addNewTabata("Diamond pushups",45);
		addNewTabata("Pause",120);
		*/
		//------------------------------------------------------------------
		
		startButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(tabataObjectList.size()>0){
					if(!running){
						((Button)v).setText(R.string.pause);
						((Button) findViewById(R.id.add)).setEnabled(false);
						running = true;
						if(timer!=null) { //to znaczy, ze to tylko po pauzie,a nie od nowa
							restartCountDown();
						}else{
							Tabata tabata = tabataObjectList.get(actualManagedIndex);
							labelTitle.setVisibility(View.VISIBLE);
							labelTitle.setText("#"+(MainTabata.this.actualManagedIndex+1)+" "+tabata.getTitle());
							//tabataListAdapter.setNrToColor(actualManagedIndex);
							//setTabataBoarder(actualManagedIndex-1, false); //tutaj musimy,bo nie przerysowujemy
							setTabataBoarder(actualManagedIndex, true);
							countDownFrom(tabata.getTime());
						}
					}else{ //Nacisnieto PAUSE
						((Button)v).setText(R.string.start);
						pauseCountDown();
						running = false;
						((Button) findViewById(R.id.add)).setEnabled(true);
					}
				}else{
					Toast.makeText(v.getContext(), R.string.emptyListToast, Toast.LENGTH_LONG).show();
				}
			}
		});
		
		addButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				generateNewEditTabataActivity(-1);
			}
		});
		
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//	        ActionBar actionBar = getActionBar();
//	        actionBar.setDisplayHomeAsUpEnabled(false);
//	        actionBar.setHomeButtonEnabled(false);
//	        actionBar.setDisplayShowTitleEnabled(false);
//	        actionBar.setNavigationMode(android.app.ActionBar.NAVIGATION_MODE_TABS);
//	        TabListener listener = new TabListener() {
//				
//				@Override
//				public void onTabUnselected(Tab tab, FragmentTransaction ft) {
//					// TODO Auto-generated method stub
//					
//				}
//				
//				@Override
//				public void onTabSelected(Tab tab, FragmentTransaction ft) {
//					// TODO Auto-generated method stub
//					
//				}
//				
//				@Override
//				public void onTabReselected(Tab tab, FragmentTransaction ft) {
//					// TODO Auto-generated method stub
//					
//				}
//			};
//			actionBar.addTab(actionBar.newTab().setText("One").setTabListener(listener ));
//	        actionBar.addTab(actionBar.newTab().setText("Two").setTabListener(listener ));
//	    }
		
	}

	
	//=======================================================================================================
	
	
//	@Override
//	public void onConfigurationChanged(Configuration newConfig) {
//		super.onConfigurationChanged(newConfig);
//	}


//	@Override
//	public void onSaveInstanceState(Bundle savedInstanceState) {
//	  // Save UI state changes to the savedInstanceState.
//	  // This bundle will be passed to onCreate if the process is
//	  // killed and restarted.
//	  savedInstanceState.putString("MyString", "Welcome back to Android");
//	  // etc.
//	  super.onSaveInstanceState(savedInstanceState);
//	}
//	
//	@Override
//	public void onRestoreInstanceState(Bundle savedInstanceState) {
//	  super.onRestoreInstanceState(savedInstanceState);
//	  // Restore UI state from the savedInstanceState.
//	  // This bundle has also been passed to onCreate.
//	  String myString = savedInstanceState.getString("MyString");
//	}
	

	@Override
	protected void onPause() {
		super.onPause();
		AppUtils.getInstance(this).setKeepScreenOn(this, false);
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		AppUtils.getInstance(this).setKeepScreenOn(this, true);
	}

	@Override
	protected void onResume() {
		super.onResume();
		AppUtils.getInstance(this).setKeepScreenOn(this, true);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		AppUtils.getInstance(this).setKeepScreenOn(this, false);
	}


	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		if (this.running)
			return;
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.add(0, ACTIVITY_SELECT, 0, R.string.contextSelect);
		menu.add(0, ACTIVITY_EDIT, 0, R.string.contextEdit);
		menu.add(0, ACTIVITY_DELETE, 0, R.string.contextDelete);
	}
	
	@Override
    public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        switch(item.getItemId()) {
            case ACTIVITY_DELETE:
            	tabataObjectList.remove((int)info.id);
            		
            	if((int)info.id<actualManagedIndex){
            		actualManagedIndex=actualManagedIndex-1;
            		labelTitle.setText("#"+(actualManagedIndex+1)+" "+tabataObjectList.get(actualManagedIndex).getTitle());
            	} else if((int)info.id==actualManagedIndex){
            		
            		if(actualManagedIndex>=tabataObjectList.size()){ //czyli wyszlosmy poza
                		manageNextTabata(false);
                		return true;
                	}
            		
            		if(timer!=null){ //czyli jest jeszcze jakas tabata
            			timer.setCountFrom(tabataObjectList.get(actualManagedIndex).getTime());
            			timer.setActualTime(0);
            		}
            		
            		labelCount.setText(AppUtils.generateTimeFromLong(tabataObjectList.get(actualManagedIndex).getTime()));
            		labelTitle.setText("#"+(actualManagedIndex+1)+" "+tabataObjectList.get(actualManagedIndex).getTitle());
            	}

            		tabataListAdapter.setNrToColor(actualManagedIndex);
            	tabataListAdapter.notifyDataSetChanged();
                setStaticTotalTime();
        	    setTotalTime();
                return true;
            case ACTIVITY_EDIT:
            	generateNewEditTabataActivity((int)info.id);
                return true;
            case ACTIVITY_SELECT:
            	startFromTabata((int)info.id);
                return true;
        }
        return super.onContextItemSelected(item);
    }
	
	
	//ON ACTIVITY RESULT
	@Override
	 protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
    	super.onActivityResult(requestCode, resultCode, intent);
    	if(resultCode==Activity.RESULT_CANCELED)
    		return;
    	
    	Bundle extras = intent.getExtras();
    	switch(requestCode) {
	    	case ACTIVITY_CREATE:
	    	    String title = extras.getString(BUNDLE_EDIT_TITLE);
	    	    long time = extras.getLong(BUNDLE_EDIT_TIME);
	    	    addNewTabata(title, time);
	    	    setStaticTotalTime();
	    	    setTotalTime();
	    	    break;
		 	case ACTIVITY_EDIT:
		 		int id = extras.getInt(BUNDLE_EDIT_ID);
		 	    Tabata edited = tabataObjectList.get(id);
		 	    edited.setTime(extras.getLong(BUNDLE_EDIT_TIME));
		 	    edited.setTitle(extras.getString(BUNDLE_EDIT_TITLE));
		 	    if(id==this.actualManagedIndex){
		 	    	if(this.timer!=null){		 	    	
		 	    		this.timer.setActualTime(0);
		 	    		this.timer.setCountFrom(extras.getLong(BUNDLE_EDIT_TIME));
		 	    	}
		 	    	labelCount.setText(AppUtils.generateTimeFromLong(extras.getLong(BUNDLE_EDIT_TIME)));
		 	    }
		 	    tabataListAdapter.setNrToColor(actualManagedIndex);
		 	    tabataListAdapter.notifyDataSetChanged();
		 	    setStaticTotalTime();
		 	    setTotalTime();
		 	    break;
		 	case ACTIVITY_SAVE:
		 		Boolean saveResult = extras.getBoolean(BUNDLE_SAVE_TITLE);
		 	    if(saveResult)
		 	    	Toast.makeText(this, R.string.cycleSaved, Toast.LENGTH_SHORT).show();
		 	    else
		 	    	Toast.makeText(this, R.string.cycleNotSaved, Toast.LENGTH_SHORT).show();
		 	    break;
		 	case ACTIVITY_OPEN: //TODO
		 		ArrayList<String> list = extras.getStringArrayList(BUNDLE_OPEN_TABATA_LIST);
		 		if(list.size()>0){
		 			clearTabataList();
		 			for(String line: list){
		 				Pattern pattern = Pattern.compile("([a-zA-Z0-9 /-_!]{1,20})=(\\d+)");
				        Matcher m = pattern.matcher(line);
				        while(m.find()){
				        	addNewTabata(m.group(1), Long.valueOf(m.group(2)));
				        }
		 			}
		 			setStaticTotalTime();
			 	    setTotalTime();
		 		}else{
		 			Toast.makeText(this, R.string.noTabatasRead, Toast.LENGTH_SHORT).show();
		 		}
		 	    break;
	 	}
        
    }
	
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if(this.running)
			return false;
		return super.onPrepareOptionsMenu(menu);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.optionsmenu, menu);
	    return true;
		
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    case R.id.menuReset:
	    	resetTabataCountdown(); //reset to start
	        return true;
	    case R.id.menuClear:
	    	askUserToClearList();
	        return true;
	    case R.id.menuOpen:
	    	generateOpenListActivity();
	    	return true;
	    case R.id.menuSave:
	    	generateSaveListActivity();
	    	return true;
	    case R.id.menuPrefs:
	    	generatePrefsActivity();
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}
	
	//=======================================================================================================
	
	private void generatePrefsActivity(){
		Intent i = new Intent(this, Prefs.class);
		startActivity(i);
	}
	
	
	private void generateNewEditTabataActivity(int id){
		Intent i = new Intent(this, AddEditTabata.class);
		if(id>=0){ //jesli podany,to edytujemy, a nie dodajemy
			Tabata tabata = this.tabataObjectList.get(id);
			Bundle extras = new Bundle();
			extras.putInt(BUNDLE_EDIT_ID, id);
			extras.putString(BUNDLE_EDIT_TITLE, tabata.getTitle());
			extras.putLong(BUNDLE_EDIT_TIME, tabata.getTime());
			i.putExtras(extras);
			startActivityForResult(i, ACTIVITY_EDIT);
		}else
			startActivityForResult(i, ACTIVITY_CREATE);
	}
	
	private void generateOpenListActivity(){
		int available = ImportExportUtility.checkAvailability();
		if(available==-1){
			Toast.makeText(this, R.string.notMountedSD, Toast.LENGTH_SHORT).show();
			return;
		}else if(available==0){
			Toast.makeText(this, R.string.cantWriteToSD, Toast.LENGTH_SHORT).show();
			return;
		}
		
		Intent i = new Intent(this, OpenListActivity.class);
		Bundle extras = new Bundle();
			//extras.putString(BUNDLE_OPEN, );
		i.putExtras(extras);
		startActivityForResult(i, ACTIVITY_OPEN);
	}
	
	private void generateSaveListActivity(){
		if(this.tabataObjectList.size()<1){
			Toast.makeText(this, R.string.emptyListToast, Toast.LENGTH_SHORT).show();
			return;
		}
		
		int available = ImportExportUtility.checkAvailability();
		if(available==-1){
			Toast.makeText(this, R.string.notMountedSD, Toast.LENGTH_SHORT).show();
			return;
		}else if(available==0){
			Toast.makeText(this, R.string.cantWriteToSD, Toast.LENGTH_SHORT).show();
			return;
		}
			
		int writing = ImportExportUtility.checkWritingPrivilages();
		if(writing==-1 || writing==0){
			Toast.makeText(this, R.string.cantWriteToSD, Toast.LENGTH_SHORT).show();
			return;
		}
		
		Intent i = new Intent(this, SaveListActivity.class);
		Bundle extras = new Bundle();
			extras.putString(BUNDLE_SAVE_TITLE, this.tabataListToString());
		i.putExtras(extras);
		startActivityForResult(i, ACTIVITY_SAVE);
	}
	
	private void countDownFrom(long countTime){
		timer = new CountTimer(this, CountType.STANDARD_COUNTDOWN, countTime, SystemClock.uptimeMillis(), mHandler, labelCount);
		timer.startIt();
	}
	
	private void pauseCountDown() {
		timer.pauseCountDown();
	}
	
	private void restartCountDown() {
		timer.restartCountDown();
	}
	
	//=======================================================================================================
	
	private void prepareSoundPlayers() {
		soundManager = new SoundManager();
		soundManager.initSounds(getBaseContext());
		
		soundManager.addSound(1, R.raw.shortsound);
		soundManager.addSound(2, R.raw.tabataendsound);
		//soundManager.addSound(3, R.raw.cycleEndSound);
	}
	
	public void playShortSound(){
		if(PreferenceManager.getDefaultSharedPreferences(this).getBoolean("soundPref", true))
			soundManager.playSound(1);
	}
	
	public void playEndTabataSound(){
		if(PreferenceManager.getDefaultSharedPreferences(this).getBoolean("soundPref", true))
			soundManager.playSound(2);
	}
	
	//=======================================================================================================
	
	public void setTabataBoarder(int id, boolean selected){
//		View tabataView = ((ListView) findViewById(R.id.listLayout)).getChildAt(id);
//		if(tabataView==null) //nie znaleziono tabaty pod takim id
//			return;
//		LinearLayout boarderLayout = (LinearLayout)tabataView.findViewById(R.id.oneTabataViewBoarder);
//		if(selected)
//			boarderLayout.setBackgroundColor(Color.parseColor("#FF0000"));
//		else
//			boarderLayout.setBackgroundColor(Color.parseColor("#00aa00"));
		if(selected)
			this.tabataListAdapter.setNrToColor(id);
		else
			this.tabataListAdapter.setNrToColor(-1);
		
		tabataListAdapter.notifyDataSetChanged();
		
		if(selected)//teraz a nie wyzej,poniewaz wczesniej jest inny view!
			this.listLayout.setSelection(id);
	}
	
	public void setAllTabatasBoarder(boolean select){
		for(int i=0; i<this.tabataObjectList.size();i++)
			setTabataBoarder(i,select);
	}
	
	private void startFromTabata(int id){
		this.actualManagedIndex = id;
		Tabata tabata = this.tabataObjectList.get(id);
		labelCount.setText(AppUtils.generateTimeFromLong(tabata.getTime()));
		updateTotalCount(tabata.getTime());
		this.actualManagedIndex = id-1;//-1, bo managedNext() dodaje na poczatku 1 !
		this.setAllTabatasBoarder(false); //to neiwiemy w sumie ktora ramke trzeb usunac
		manageNextTabata(false);
	}
	
	private void addNewTabata(String title, long time){
		Tabata newTabata=new Tabata(title, time);
		tabataObjectList.add(newTabata);
		tabataListAdapter.setNrToColor(actualManagedIndex);
		tabataListAdapter.notifyDataSetChanged();
	}
	
	
	public void manageNextTabata(boolean start){
		actualManagedIndex++;
		timer = null;
		if(actualManagedIndex>=this.tabataObjectList.size()){
			setTabataBoarder(actualManagedIndex-1, false); //tutaj musimy,bo nie przerysowujemy
			//dzwiek koncowy
			this.playEndTabataSound();
			resetTabataCountdown();
			Toast.makeText(this, R.string.endOfCycleToast, Toast.LENGTH_SHORT).show();
			return;
		}
		Tabata tabata = tabataObjectList.get(actualManagedIndex);
		labelTitle.setVisibility(View.VISIBLE);
		labelTitle.setText("#"+(actualManagedIndex+1)+" "+tabata.getTitle());
		//this.tabataListAdapter.setNrToColor(actualManagedIndex);
		//setTabataBoarder(actualManagedIndex-1, false);
		setTabataBoarder(actualManagedIndex, true);
		
		if(start)
			countDownFrom(tabata.getTime());
	}


	public void setTotalTime(){
		long sumTime = getSummaryTabataTime(this.actualManagedIndex);
		if(timer!=null)
			sumTime = sumTime - timer.getActualTime();
		labelTotalCount.setText(AppUtils.generateTimeFromLong(sumTime));
	}
	
	public void setStaticTotalTime(){
		long sumTime = getSummaryTabataTime(-1);
		((TextView)findViewById(R.id.label_static_total)).setText(AppUtils.generateTimeFromLong(sumTime));
	}
	
	private long getSummaryTabataTime(int from){
		long sumTime = 0;
		if(from<0)
			from=0;
		for(int i=from; i<tabataObjectList.size(); i++){
			Tabata tabata = tabataObjectList.get(i);
			sumTime = sumTime + tabata.getTime();
		}
		return sumTime;
	}
	
	public void updateTotalCount(long time_left_for_actual){
		long sumTime = time_left_for_actual;
		//sumuj od nastepnej do konca
		for(int i=(this.actualManagedIndex+1); i<this.tabataObjectList.size();i++){
			Tabata tabata = this.tabataObjectList.get(i);
			sumTime = sumTime + tabata.getTime();
		}
		labelTotalCount.setText(AppUtils.generateTimeFromLong(sumTime));
	}
	
	private void resetTabataCountdown(){
		((Button)findViewById(R.id.sp)).setText(R.string.start);
		((Button) findViewById(R.id.add)).setEnabled(true);
		labelCount.setText(R.string.zeroTime);
		labelTitle.setVisibility(View.GONE);
		this.actualManagedIndex = 0;
		
		//this.setAllTabatasBoarder(false);
    	setTabataBoarder(actualManagedIndex,true);
    	
		mHandler.removeCallbacks(timer);
		timer=null;
		running=false;
		this.setTotalTime();
		this.setStaticTotalTime();
	}
	
	private void clearTabataList(){
		this.tabataObjectList.clear(); //clear whole list;
		this.tabataListAdapter.notifyDataSetChanged(); //update adapter
		this.tabataListAdapter.setNrToColor(0);
		
		resetTabataCountdown(); //reset to start
	}
	
	private void askUserToClearList(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(R.string.dialogClearText)
		       .setCancelable(false)
		       .setPositiveButton(R.string.dialogClearYes, new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		                MainTabata.this.clearTabataList();
		           }
		       })
		       .setNegativeButton(R.string.dialogClearNo, new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		                dialog.cancel();
		           }
		       });
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	
	//=========================== SAVE ================================
	
	private String tabataListToString(){
		String eol = System.getProperty("line.separator");
		String ret = "";
		for(Tabata tabata: tabataObjectList)
			ret = ret + tabata.getTitle() + "=" + tabata.getTime() + eol;
		
		return ret;
	}
	
	public ArrayList<Tabata> getTabataObjectList(){
		return this.tabataObjectList;
	}
}