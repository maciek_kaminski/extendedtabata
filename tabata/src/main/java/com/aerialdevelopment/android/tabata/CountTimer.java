package com.aerialdevelopment.android.tabata;

import android.os.Handler;
import android.os.SystemClock;
import android.widget.TextView;

public class CountTimer implements Runnable {
		
		private MainTabata parent;
		private long actualTime;
		private CountType type;
		private long countFrom;
		private long startTime;
		private Handler mHandler;
		private TextView label;
		
		public CountTimer(MainTabata parent, CountType type, long countFrom,
				long startTime, Handler mHandler, TextView label) {
			super();
			this.parent = parent;
			this.type = type;
			this.countFrom = countFrom;
			this.startTime = startTime;
			this.mHandler = mHandler;
			this.label = label;
			
			//first use..
			this.setActualTime(0);
		}

		public void setmParent(MainTabata parent) {
			this.parent = parent;
		}
		
		public void setmHandler(Handler mHandler) {
			this.mHandler = mHandler;
		}

		public void setLabel(TextView label) {
			this.label = label;
		}
		
		public void setType(CountType type) {
			this.type = type;
		}

		public void setCountFrom(long countFrom) {
			this.countFrom = countFrom;
		}

		public void setStartTime(long startTime) {
			this.startTime = startTime;
		}

		public long getActualTime() {
			return actualTime;
		}

		public void setActualTime(long actualTime) {
			this.actualTime = actualTime;
		}
		
		public void pauseCountDown(){
			mHandler.removeCallbacks(this);
		}
		
		public void restartCountDown(){
			this.setCountFrom(this.countFrom-this.getActualTime());
			this.setStartTime(SystemClock.uptimeMillis());
			this.startIt();
		}
		
		//MAIN START
		public void startIt(){
			mHandler.removeCallbacks(this);
			mHandler.postDelayed(this, 1);
		}

		//MAIN COUNTDOWN
		public void run() {
			long millis = SystemClock.uptimeMillis() - this.startTime;
			int seconds = (int) (millis / 1000);
			this.setActualTime(seconds);
			if(seconds > this.countFrom){
				if(type.equals(CountType.STANDARD_COUNTDOWN)){
					mHandler.removeCallbacks(this);
					parent.manageNextTabata(true);
					return;
				}
			}
			
			if(type.equals(CountType.STANDARD_COUNTDOWN)){
				long diff = countFrom-seconds;
				if(diff<4 && diff !=0)
					parent.playShortSound();
				else if(diff <= 0)
					parent.playEndTabataSound();
				label.setText(AppUtils.generateTimeFromLong(diff));
				label.setText(AppUtils.generateTimeFromLong(diff));
				parent.updateTotalCount(diff);
			}
			
			mHandler.postAtTime(this, this.startTime + ((seconds + 1) * 1000));
		}
}
