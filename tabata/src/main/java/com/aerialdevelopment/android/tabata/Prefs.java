package com.aerialdevelopment.android.tabata;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class Prefs extends PreferenceActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.prefs);
		
		//default values
		//PreferenceManager.setDefaultValues(this, R.xml.prefs, true);
	}

}
