package com.aerialdevelopment.android.tabata;

import android.content.Context;
import android.os.PowerManager;

public class Settings {
	
	Context mContext;
	
	public Settings(Context ctx) {
		this.mContext = ctx;
	}

	public void setScreenOn(){
		PowerManager pm = (PowerManager) mContext
				.getSystemService(Context.POWER_SERVICE);
		PowerManager.WakeLock wl = pm.newWakeLock(
				PowerManager.SCREEN_DIM_WAKE_LOCK
						| PowerManager.ON_AFTER_RELEASE, "ExtendedTabata");
		wl.acquire();
		// ...
		wl.release();
	}
}
