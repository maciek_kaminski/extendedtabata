package com.aerialdevelopment.android.tabata;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.WindowManager;

public class AppUtils {

    public static int SECONDS_IN_MINUTE = 60;

    private static AppUtils instance = null;
	SharedPreferences prefs = null;
	
	public AppUtils(Context context){
		prefs = PreferenceManager.getDefaultSharedPreferences(context);
	}
	
	public static AppUtils getInstance(Context context){
		if(instance==null){
			instance = new AppUtils(context);
		}
		return instance;
	}
	
	public void setKeepScreenOn(Activity activity, boolean keepScreenOn) {
		if(getBacklightPref() && keepScreenOn) {
			activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		} else {
			activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}
		
	}


    public static String generateTimeFromLong(long time) {
        long minutes = time / AppUtils.SECONDS_IN_MINUTE;
        long seconds = time % AppUtils.SECONDS_IN_MINUTE;

        return (minutes<10 ? "0" : "") + minutes + ":" + (seconds<10 ? "0" : "") + seconds;
    }

	private boolean getBacklightPref(){
		Boolean backlightPref = prefs.getBoolean("backlightPref", true);
		return backlightPref;
	}
}
